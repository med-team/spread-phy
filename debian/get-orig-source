#!/bin/sh
# unpack spread tarball to get rid of several binaries
set -e
NAME=`dpkg-parsechangelog | awk '/^Source/ { print $2 }'`
UPSTREAMNAME='Spread-phy'

if ! echo $@ | grep -q upstream-version ; then
    VERSION=`dpkg-parsechangelog | awk '/^Version:/ { print $2 }' | sed 's/\([0-9\.]\+\)-[0-9]\+$/\1/'`
    uscan --force-download
else
    VERSION=`echo $@ | sed "s?^.*--upstream-version \([0-9.]\+\) .*${NAME}.*?\1?"`
    if echo "$VERSION" | grep -q "upstream-version" ; then
        echo "Unable to parse version number"
        exit
    fi
fi

TARDIR=${UPSTREAMNAME}-${VERSION}
mkdir -p ../tarballs
cd ../tarballs
# need to clean up the tarballs dir first because upstream tarball might
# contain a directory with unpredictable name
rm -rf *
tar -xaf ../${TARDIR}.tar.gz

UPSTREAMTARDIR=`find . -mindepth 1 -maxdepth 1 -type d`
if [ "${UPSTREAMTARDIR}" != "${TARDIR}" ] ; then
    mv "${UPSTREAMTARDIR}" "${TARDIR}"
fi

# Remove useless binaries
rm -rf ${TARDIR}/release/Mac
rm -rf ${TARDIR}/release/Windows
rm -rf ${TARDIR}/release/tools
rm -rf ${TARDIR}/bin
rm -rf ${TARDIR}/classes

# Remove debian packaged jars
rm -rf ${TARDIR}/lib/*

# Remove .git dir
rm -rf ${TARDIR}/.git

GZIP="--best --no-name" tar --owner=root --group=root --mode=a+rX -caf "$NAME"_"$VERSION".orig.tar.xz "${TARDIR}"
rm -rf ${TARDIR}
