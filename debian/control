Source: spread-phy
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               javahelper,
               ant,
               ant-optional,
               default-jdk,
               libjebl2-java,
               libcolt-free-java,
               libprocessing-core-java,
               libfest-assert-java
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/spread-phy
Vcs-Git: https://salsa.debian.org/med-team/spread-phy.git
Homepage: https://www.kuleuven.be/aidslab/phylogeography/SPREAD.html
Rules-Requires-Root: no

Package: spread-phy
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${java:Depends},
         default-jre
Conflicts: phy-spread
Provides: phy-spread
Replaces: phy-spread
Description: analyze and visualize phylogeographic reconstructions
 SPREAD is a user-friendly application to analyze and visualize
 phylogeographic reconstructions resulting from Bayesian inference of
 spatio-temporal diffusion.
 .
 There is a tutorial for SPREAD online at
 http://www.kuleuven.be/aidslab/phylogeography/tutorial/spread_tutorial.html
 .
 Originally this program is named "spread".  However, there is just such a
 package inside Debian and thus a 'phy' for phylogeny was prepended.
